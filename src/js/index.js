"use strict";

import LoanCalculator from './LoanCalculator';

// CSS
import '../css/bootstrap.min.css';
import '../css/custom.css';


let loanCalObj = new LoanCalculator(0.25);


// Document ready event
document.addEventListener('DOMContentLoaded', function () {
    const form = document.querySelector('#formCal');
    const selDate = document.querySelector('#selDate');
    const resultContent = document.querySelector('#result');
    // Display the default slider value
   const sliderAmount = document.querySelector('#sliderAmount');
   const outputAm = document.querySelector('#amountValue');
    outputAm.innerHTML = sliderAmount.value + " €";    

    // Update the current slider value each time you drag the slider handle
    sliderAmount.oninput =  function() {
        outputAm.innerHTML = this.value + " €";
    }

    const sliderPeriod = document.querySelector('#sliderPeriod');
    const outputPer = document.querySelector('#periodValue');
    outputPer.innerHTML = sliderPeriod.value + " Days";

    sliderPeriod.oninput =  function() {
        outputPer.innerHTML = this.value + " Days";
        //Enable select pay date or Disable it
        if (this.value >= 30) {
            selDate.disabled = false;
            selDate.required = true;
        }
        else if (this.value < 30) {
            selDate.disabled = true;
            selDate.required = false;
            selDate.value = "";
        }
    }

    form.addEventListener('submit', function (e) {
        let result;
        e.preventDefault();
        if (selDate.value) {
            result = loanCalObj.calculateInterest(parseInt(sliderAmount.value), parseInt(sliderPeriod.value), parseInt(selDate.value));
            resultContent.innerHTML = result;
        }
        else {
            result = loanCalObj.calculateInterest(parseInt(sliderAmount.value), parseInt(sliderPeriod.value));
            resultContent.innerHTML = result;
        }

    });

});



