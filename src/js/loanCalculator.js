"use strict";

class LoanCalculator {
    constructor(rate) {
        this.rate = rate
    }

    calculateInterest = (amount, period, date = 0) => {

        let result = "";
        let interest = amount * this.rate * (period / 365);
        let totalAmount = interest + amount;
        let numInstallments;

        if (period < 30) {
            result = `Amount of credit: £${amount}. Interest: £${interest.toFixed(2)}  Interest rate: 25% per Year (fixed). Total amount payable: £${totalAmount.toFixed(2)}`;
        }
        else if (period > 30) {
            let months = period / 30;
            numInstallments = Math.round(months);
            let payment = totalAmount / numInstallments;
            result= `Amount of credit: £${amount}. Interest: £${interest.toFixed(2)} Interest rate: 25% per Year (fixed). Total amount payable: £${totalAmount.toFixed(2)} Repay with ${numInstallments} payments: every payment is: ${payment.toFixed(2)} from ${date} next month.`;
        }
        //console.log(result);
        return result;
    }

}

export default LoanCalculator;