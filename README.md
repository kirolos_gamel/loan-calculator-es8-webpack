# 🚀 Welcome to Loan Calculator dashboard project!

This project has been created using **webpack scaffold**, you can now run

```
npm run build
```

or

```
yarn build
```

to bundle your application

# Developer: Kirolos Ibrahim

# Description of project:

```
Create a simple single page application with fixed-date loan repayment calculator.
As an example you could use existing calculator at https://www.creditstar.co.uk/ homepage.
Calculator must have these input fields:
1.	Loan amount (eg. 1000.00 EUR)
2.	Loan period (eg. 5 months)
3.	Loan repayment date (eg. every 31st day of the month)
Calculator should display:
1.	Number of instalments
2.	Date of each instalment
3.	Amount of each instalment
4.	Interest part of each instalment
Assume that the interest rate is fixed: 25% (per annum)

```
#Technologies:

```
HTML - CSS - CSS3 - BOOTSTRAP FRAMEWORK - JAVASCRIPT VANILLA ES6 - WEBPACK 
```

